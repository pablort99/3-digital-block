﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleScript : MonoBehaviour
{

    public float speed = 0.0f;
    public float distance = 0.0f;
    public float time = 0.0f;

    public float maxSpeedLimit=70.0f;
    public float minSpeedLimit = 40.0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SpeedCheck();
        }
    }

    void SpeedCheck()
    {
        speed = distance / time;

        if (speed >= maxSpeedLimit)
        {
            print("you are exceeding the speed limit!");
        }
        else if (speed <= minSpeedLimit)
        {
            print("you are not going fast enough!");
        }
        else if (speed == maxSpeedLimit || speed == minSpeedLimit)
        {
            print("you are very close to the limit");
        }
        else
        {
            print("you are within the speed limit!");
        }
       

    }
}
